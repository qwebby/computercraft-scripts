-- CONFIGURE ME
local redStoneOutputSide = "RIGHT" -- Ensure they are all oriented the same way
local fansNumber = "0"
local grindersNumber = "0"
local spawnersNumber = "0"
local duplicatorNumber = "0"
-- END CONFIGURE ME


-- Docs: https://basalt.madefor.cc/#/
-- Check if basalt web exists, if not download
if not fs.exists("basaltWeb.lua") then
    print("Downloading libraries...")
    shell.run("wget", "run", "https://basalt.madefor.cc/install.lua", "web", "latest.lua", "basaltWeb.lua")
    print("Finished downloading libraries.")
end

-- Setup Basalt
local basalt = require("basaltWeb")
basalt.setTheme({
    BaseFrameBG = colors.black,
    BaseFrameText = colors.white,
})

-- If montior is present, use it
local main = nil
local monitor = peripheral.find("monitor")
if monitor then
    main = basalt.addMonitor()
    main:setMonitor(monitor)
else 
    main = basalt.createFrame()
end

local w, h = term.getSize()

local REDSTONE_INTEGRATOR = "redstoneIntegrator_"
local fans = peripheral.wrap(REDSTONE_INTEGRATOR .. fansNumber)
local grinders = peripheral.wrap(REDSTONE_INTEGRATOR .. grindersNumber)
local spawners = peripheral.wrap(REDSTONE_INTEGRATOR .. spawnersNumber)
local duplicator = peripheral.wrap(REDSTONE_INTEGRATOR .. duplicatorNumber)

local lblDuplicator = "MOB DUPER"
local lblSpawners = "SPAWNERS"
local lblGrinders = "GRINDERS"
local lblFans = "FANS"
local lblToggleAll = "TOGGLE ALL"

local isDuplicatorEnabled = false
local isSpawnerEnabled = false
local isGrinderEnabled = false
local isFansEnabled = false
local isAllEnabled = false

local onColor = colors.lightGray
local offColor = colors.black

-- Turn all outputs off by default when program starts 
if fans then
    fans.setOutput(redStoneOutputSide, false)
end
if grinders then 
    grinders.setOutput(redStoneOutputSide, false)
end
if spawners then
    spawners.setOutput(redStoneOutputSide, false)
end
if duplicator then
    duplicator.setOutput(redStoneOutputSide, false)
end

local aLabel = main:addLabel()
    :setText("MEATBALL'S SPAWNER CONTROLS")
    :setFontSize(1)

-- Button definitions
local buttonSizeX = 12
local buttonSizeY = 3
local btnDuplicator = main:addButton()
    :setPosition(2,3)
    :setSize(buttonSizeX,buttonSizeY)
    :setText(lblDuplicator)
    :setBorder(colors.white, "top", "left", "right", "bottom")
    :setBackground(offColor)
local btnGrinders = main:addButton()
    :setPosition(2,7)
    :setSize(buttonSizeX,buttonSizeY)
    :setText(lblGrinders)
    :setBorder(colors.white, "top", "left", "right", "bottom")
    :setBackground(offColor)

local btnSpawners = main:addButton()
    :setPosition(17,3)
    :setSize(buttonSizeX,buttonSizeY)
    :setText(lblSpawners)
    :setBorder(colors.white, "top", "left", "right", "bottom")
    :setBackground(offColor)
local btnFans = main:addButton()
    :setPosition(17,7)
    :setSize(buttonSizeX,buttonSizeY)
    :setText(lblFans)
    :setBorder(colors.white, "top", "left", "right", "bottom")
    :setBackground(offColor)

local btnAll = main:addButton()
    :setPosition(10, 11)
    :setSize(buttonSizeX, 1)
    :setText(lblToggleAll)
    :setBorder(colors.white, "left", "right")
    :setBackground(offColor)


-- Set Values, background color, and redstone output to modems
function SetGrinders(isEnabled)
    isGrinderEnabled = isEnabled
    btnGrinders:setBackground(isEnabled and onColor or offColor)
    if grinders then
        grinders.setOutput(redStoneOutputSide, isEnabled)
    end
end

function SetFans(isEnabled)
    isFansEnabled = isEnabled
    btnFans:setBackground(isEnabled and onColor or offColor)
    if fans then
        fans.setOutput(redStoneOutputSide, isEnabled)
    end
end

function SetSpawners(isEnabled)
    isSpawnerEnabled = isEnabled
    btnSpawners:setBackground(isEnabled and onColor or offColor)
    if spawners then
        spawners.setOutput(redStoneOutputSide, isEnabled)
    end
end

function SetDuplicator(isEnabled)
    isDuplicatorEnabled = isEnabled
    btnDuplicator:setBackground(isEnabled and onColor or offColor)
    if duplicator then
        duplicator.setOutput(redStoneOutputSide, isEnabled)
    end
end

function SetToggleAll(isEnabled)
    isAllEnabled = isEnabled
    -- If any on then turn all off
    if isDuplicatorEnabled or isSpawnerEnabled or isGrinderEnabled or isFansEnabled then
        isAllEnabled = false
        btnAll:setBackground(offColor)
        SetGrinders(false)
        SetFans(false)
        SetSpawners(false)
        SetDuplicator(false)
    else
        SetGrinders(isEnabled)
        SetFans(isEnabled)
        SetSpawners(isEnabled)
        SetDuplicator(isEnabled)
        btnAll:setBackground(onColor)
    end
end


-- Desc: This is the main handle change
--  Which changes the redstone output based on the label value
-- Params:
--  - self: The actual checkbox passed by refernce(?)
local function handleButtonClick(self)
    -- Set up values used in function
    -- local isChecked = not self:getValue()
    local labelValue = self:getText()

    if labelValue == lblDuplicator then
        SetDuplicator(not isDuplicatorEnabled)
    elseif labelValue == lblSpawners then
        SetSpawners(not isSpawnerEnabled)
    elseif labelValue == lblGrinders then
        SetGrinders(not isGrinderEnabled)
    elseif labelValue == lblFans then
        SetFans(not isFansEnabled)
    elseif labelValue == lblToggleAll then
        SetToggleAll(not isAllEnabled)
    end
end

-- These are defined down here so that the handle func
--  can call to the set functions
btnDuplicator:onClick(handleButtonClick)
btnSpawners:onClick(handleButtonClick)
btnGrinders:onClick(handleButtonClick)
btnFans:onClick(handleButtonClick)
btnAll:onClick(handleButtonClick)

-- DO NOT REMOVE --
basalt.autoUpdate()