-- Func get input
function get_input()
	print("Enter stair depth:")
	depth = tonumber(io.read())
	depth = depth or 1
	return depth
end

function dig_stairs(depth)
	for i = 1, depth do
		print("digging " .. i .. " of " .. depth)
		turtle.dig()
		turtle.forward()
		turtle.dig()
		turtle.digDown()
		turtle.down()
	end
end

function main()
	turtle.refuel()
	local depth = get_input()
	dig_stairs(depth)
end

main()