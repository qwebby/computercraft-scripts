-- START CONFIG
local OWNER = "" -- Your username
local LOCATION = "" -- The current room name
local RADIUS = 5 -- Circular Block Radius(Place in center of room)
local DOWNTIME_BEFORE_RESET = 4 -- Seconds to wait before sending report, and reset
local DISCORD_WEBHOOK_URL = "" -- Your Discord webhook url
-- END CONFIG

local DiscordHook = require("DiscordHook")
local isOwnerInRange = false
local detector = peripheral.find("playerDetector")
local resetTimerCtr = 0
local playersSeen = 0

detectedPlayerList = {}

while true do
    playersInRange = detector.getPlayersInRange(RADIUS)
    while #playersInRange > 0 do
        os.sleep(1)
        -- Ignore owner
        for key, value in pairs(playersInRange) do
            if value == OWNER then
                isOwnerInRange = true
            end
        end
        
        -- If owner is in range then ignore, else start tracking
        if not isOwnerInRange then

            for index, playerName in pairs(playersInRange) do
                newDectectedPlayer = nil
                
                if detectedPlayerList[playerName] == nil then
                    --print("DBG: Player not in list, adding..")
                    detectedPlayerList[playerName] = { name = playerName, timeSpentInLocation = 0 }
                    newDectectedPlayer = detectedPlayerList[playerName]
                    playersSeen = playersSeen + 1
                else
                    --print("DBG: Player already in list, updating..")
                    newDectectedPlayer = detectedPlayerList[playerName]
                end

                
                newDectectedPlayer.timeSpentInLocation = newDectectedPlayer.timeSpentInLocation + 1
                detectedPlayerList[playerName] = newDectectedPlayer
                -- print("DBG: " .. newDectectedPlayer.name .. " spent " .. newDectectedPlayer.timeSpentInLocation .. " seconds in " .. LOCATION)
            end
        end
        playersInRange = detector.getPlayersInRange(RADIUS)
    end

    os.sleep(DOWNTIME_BEFORE_RESET)

    if playersSeen > 0 then
        print("INFO: Sending Report...")
        local success, hook = DiscordHook.createWebhook(DISCORD_WEBHOOK_URL)
        if not success then
            error("Webhook connection failed! Reason: " .. hook)
        end

        for index, value in pairs(detectedPlayerList) do
            title = "Player Detector Turtle"
            msg = "`" .. value.name .. "` spent `" .. value.timeSpentInLocation .. "` seconds in `" .. LOCATION .. "`"
            hook.send(msg, title)
        end

        detectedPlayerList = {}
        playersSeen = 0
    end
end